import React, { useState } from 'react';

export const UserContext = React.createContext();

export const UserProvider = ({children}) => {

    const [user, setUser] = useState();
    const [token, setToken] = useState({token: sessionStorage.getItem('token')})    // token is set during the Login

    return(
        <UserContext.Provider value = {{user, setUser, token, setToken}} >
            {children}
        </UserContext.Provider>
    )

}
