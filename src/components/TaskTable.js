import React, { useState, useEffect } from "react";
import { Container, Table } from "react-bootstrap";

export default function TaskTable({ taskDetailsProp, taskTableProps }) {
    const { taskId, breakdown } = taskDetailsProp;
    const [tasks, setTasks] = useState([]);

    const [hover, setHover] = useState(false);

    const onMouseEnter = () => setHover(true);
    const onMouseLeave = () => setHover(false)

    useEffect(() => {
        if (breakdown) {
            setTasks(breakdown);
        }
    }, [breakdown]);

    const handleCheckbox = (weekTaskId, checkboxValue) => {
        setTasks(prevTasks =>
            prevTasks.map(task =>
                task._id === weekTaskId ? { ...task, isPaid: checkboxValue } : task
            )
        );
        updatePaymentStatus(weekTaskId, checkboxValue);
    };

    const updatePaymentStatus = (weekTaskId, paymentStatus) => {

        fetch(`${process.env.REACT_APP_baseURL}/tasks/${taskId}/${weekTaskId}`, {
            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${sessionStorage.getItem('token')}`,
                "Content-Type": 'application/json'
            },
            body: JSON.stringify({
                isPaid: paymentStatus
            })
        })
        .then(result => result.json())
        .then(data => {
            // console.log("data: ", data);
            taskTableProps(data);
            
        })
        .catch(err => console.error("Error updating payment status: ", err));
    };

    return (
        <Container>
            <div style={{maxHeight: "100vh", overflowY: "auto", textAlign: "center"}}>
                <Table striped bordered hover >
                    <thead>
                        <tr style={{top: 0, zIndex: 1, position: "sticky"}}>
                            <th>Week</th>
                            <th>To Pay</th>
                            <th>Total</th>
                            <th>Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tasks.map(weekTask => (
                            <tr key={weekTask._id} >
                                {weekTask.isPaid?
                                <>
                                <td className="table-body-data" onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>{weekTask.week}</td>
                                <td className="table-body-data">{weekTask.taskExpectedAmount}</td>
                                <td className="table-body-data">{weekTask.taskTotalAmount}</td>
                                <td className="table-body-data">
                                    <input
                                        type="checkbox"
                                        checked={weekTask.isPaid}
                                        onChange={e => handleCheckbox(weekTask._id, e.target.checked)}
                                    />
                                </td>
                                </>
                                :
                                <>
                                <td >{weekTask.week}</td>
                                <td>{weekTask.taskExpectedAmount}</td>
                                <td>{weekTask.taskTotalAmount}</td>
                                <td>
                                    <input
                                        type="checkbox"
                                        checked={weekTask.isPaid}
                                        onChange={e => handleCheckbox(weekTask._id, e.target.checked)}
                                    />
                                </td>
                                </>
                                }
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        </Container>
    );
}
