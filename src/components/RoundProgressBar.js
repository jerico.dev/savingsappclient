import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 200px;
  width: 200px;
  position: relative;
`;

const Circle = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background: conic-gradient(
    ${props => props.color} ${props => props.progress}%, 
    #e0e0e0 ${props => props.progress}%
  );
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  font-weight: bold;
  color: ${props => props.textColor};
  transition: color 0.6s ease; /* Add a smooth transition effect */
`;

const RoundedProgressBar = ({ progress, color = 'black' }) => {
  let textColor;

  switch (true) {
    case progress > 90:
      textColor = '#24F22D'; // white if progress > 75
      break;
    case progress > 80:
      textColor = '#62F224'; // orange if progress > 50
      break;
    case progress > 70:
      textColor = '#8BF224'; // blue if progress > 25
      break;
    case progress > 60:
      textColor = '#A7F224';
      break;
    case progress > 50:
      textColor = '#DFF80E';
      break;
    case progress > 40:
      textColor = '#E9F962';
      break;
    case progress > 30:
      textColor = '#F0F8AB';
      break;
    default:
      textColor = '#E7FCA0'; // black otherwise
      break;
  }

  return (
    <Container>
      <Circle progress={progress} color={textColor} textColor={color}>
        {progress}%
      </Circle>
    </Container>
  );
};

export default RoundedProgressBar;
