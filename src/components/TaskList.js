import { useEffect, useState } from "react";

import { Modal, Button } from "react-bootstrap";

import { useContext } from "react";

import { UserContext } from '../UserContext';
import TaskDetails from "./TaskDetails";


export default function TaskList ({selectedTaskProp, taskTableProps}) {

    const [description, setDescription] = useState("");
    const [increment, setIncrement] = useState();
    const [startDate, setStartDate] = useState();
    const [endDate, setEndDate] = useState();
    const [isButtonActive, setIsButtonActive] = useState(false);
    const [taskInfo, setTaskInfo] = useState();
    const [isTaskArrayEmpty, setIsTaskArrayEmpty] = useState();
    const {user, setUser} = useContext(UserContext);

    const [show, setShow] = useState(false);

    const handleShow = () => setShow(true);         // this function is called by the Modal 
    
    const handleClose = () => {                     // called by the "Cancel" button, empties fields to avoid "Submit" button malfunction
        setDescription("");
        setIncrement();
        setStartDate();   
        setEndDate();     
        setShow(false)
    };
    
    // Function to create the task
    const createTask = async (e) => {

        e.preventDefault();

        await fetch(`${process.env.REACT_APP_baseURL}/tasks/createTask`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('token')}`, 
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                description: description,
                increment: increment,
                startDate: startDate,
                endDate: endDate
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data._id){
                console.log("data id ", data._id)
                
                handleClose()
            }
            else{
                console.log("Data ID: ", data._id)
            }

        })
        .catch(err => console.error('Error creating task: ', err))
        
        
    }

    // displays the specific task recently created
    const displayUserTask = async (taskId) => {
        console.log("taskInfo:", taskId)

        await fetch(`${process.env.REACT_APP_baseURL}/tasks/${taskId}`, {
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('token')}`, 
                'Content-Type': 'application/json'
            }
        })
        .then(result => result.json())
        .then(data => {
            // console.log('DISPLAYED: ', data);
            data.map(taskToDisplay => {
                selectedTaskProp(taskToDisplay)
            })
        })
        .catch(err => console.error("Error displaying User Task: ", err))
        
    }

   
    // Fetch the user data
    const getUserProfile = async () => {
        
        await fetch(`${process.env.REACT_APP_baseURL}/users/profile`, {
            headers: {
                'Authorization': `Bearer ${sessionStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => setTaskInfo(data.tasks.reverse().map(taskArray => {
            return(
                <div key={taskArray.taskId} >
                    <div role="button" onClick={() => displayUserTask(taskArray.taskId)}>{taskArray.taskDescription}</div>
                </div>
            )
        })))
        .catch(err => console.error("Error fetching user profile: ", err))
    }


    useEffect(() => {
        
        if(description === "" || increment === "" || startDate === undefined || endDate === undefined){
            
            setIsButtonActive(false);       // if any of the field is empty, the button will be disabled
        }
        else{
            let formattedStart = new Date(startDate)
            let formattedEnd = new Date(endDate)
            let diff = (formattedEnd-formattedStart)/(1000*3600*24);
            
            // to get specific date range for expected calculation of the amounts, range can only be divisible by 7 days
            if(diff % 7 === 0){

                setIsButtonActive(true);    // activates the "Submit" button

            }
            else{
                
                setIsButtonActive(false);   // disables the "Submit" button

            }
            
        }
    }, [description, increment, startDate, endDate])

    useEffect(() => {
        getUserProfile()
        
    })

    return(
        <>
        <div id="task-list-div">
            <button id="task-list-create-button" onClick={handleShow}>Create</button>       {/* initial button -- this calls the modal */}
            <div id="task-description-div" className="bg-dark text-warning">{taskInfo.reverse()}</div>
        </div>
  

        {/* Modal */}
        <Modal show={show} onHide={handleClose} className="p-5">
            <Modal.Header closeButton>
                <Modal.Title>Create your task</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form onSubmit={e => createTask(e)}>
                    <input 
                        type="text" 
                        placeholder="Enter task description"
                        onChange={e => setDescription(e.target.value)}
                        required
                    />
                    <input 
                        type="number" 
                        placeholder="Desired weekly increment"
                        onChange={e => setIncrement(parseInt(e.target.value))}
                        required
                    />
                    <input
                        type="date" 
                        onChange={e=>setStartDate(e.target.value)}
                        required
                    />
                    <input
                        type="date" 
                        onChange={e=>setEndDate(e.target.value)}
                        required
                    />
                    {isButtonActive?
                        <Button variant="primary" type="submit">Create</Button>
                        :
                        <Button variant="primary" disabled>Create</Button>
                    }
                    <Button variant="secondary" onClick={handleClose}>Cancel</Button>
                </form>
            </Modal.Body>
        </Modal>
        </>
    )

}