import { useEffect, useState, useReducer } from "react";
import RoundedProgressBar from "./RoundProgressBar";

export default function TaskDetails ({taskDetailsProp, taskTableProps}) {
    
    const {taskId, taskDescription, taskBalance, taskDuration, taskStartDate, taskEndDate} = taskDetailsProp;
    console.log(taskTableProps.breakdown.filter(stat => stat.isPaid ).length)
    const [progress, setProgress] = useState(0);
    const [paidCount, setPaidCount] = useState(0)
    const [breakdownArrayLength, setBreakdownArrayLength] = useState(0)


    useEffect(() => {
        setProgress(taskTableProps.progress)
    }, [taskTableProps]);
    
    return(
        <>
        <div>
            <div>
                <div>Description: {taskDescription}</div>
                <div>Balance: {taskBalance}</div>
                <div>Duration: {taskDuration} weeks</div>
                <div>Start: {new Date(taskStartDate).toLocaleDateString()}</div>
                <div>End: {new Date(taskEndDate).toLocaleDateString()}</div>
            </div>
        </div>
        <div>
            <RoundedProgressBar progress={progress} />
        </div>
        </>
    ) 
}