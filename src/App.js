import './App.css';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';

import { UserContext, UserProvider } from './UserContext';

import Login from './pages/Login';
import Register from './pages/Register';
import TaskList from './components/TaskList';
import Task from './pages/Task';

function App() {
  
  return (
    <UserProvider>
      <div id="App">
        <Router>
          <Routes>
            <Route path="/register" element={<Register />} />
            <Route path="/" element={<Login />}/>
            <Route path="/tasks" element={<Task/>} />
            {/* <Route path="/tasksList" element={<TaskList/>} /> */}
          </Routes>
        </Router>
      </div>
    </UserProvider>
  );
}

export default App;
