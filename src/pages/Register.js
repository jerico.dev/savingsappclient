import '../App.css';

import { useState, useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';

export default function Register () {

    const [firstName, setFirstname] = useState('');
    const [lastName, setLastName] = useState('');
    const [userName, setUserName] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [areFieldsComplete, setAreFieldsComplete] = useState(false);
    const [isRegistered, setIsRegistered] = useState(false);
    const [message, setMessage] = useState('');

    const navigate = useNavigate();
    
    
    const registerUser = (e) => {

        e.preventDefault();

        fetch(`${process.env.REACT_APP_baseURL}/users/registration`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                userName: userName,
                password: password1
            })

        })
        .then(res => res.json())
        .then(data => {
            if(data._id){
                setIsRegistered(true);
                setMessage('');
                // navigate('/')
            }
            else{
                setIsRegistered(false);
                setMessage(data);
            }
        })
        .catch(err => console.error("Registration error: ", err))
                  
    }

    useEffect(() => {
        if((firstName !== '' && lastName!== '' && userName !== '' && password1 !== '')  && password1===password2){
            setAreFieldsComplete(true);
        }
        else{
            setAreFieldsComplete(false);
        }
    }, [firstName, lastName, userName, password1, password2])

    return (
        <div id="form-div" >
            <form id="register-form" onSubmit={e => registerUser(e)}>
                <input 
                    type="text" 
                    placeholder="First name (special characters are not accepted)" 
                    value={firstName} 
                    onChange={e => {setFirstname(e.target.value)}} 
                    pattern="[A-Z a-z]"
                    required
                />
                <input 
                    type="text" 
                    placeholder="Last name (special characters are not accepted)" 
                    value={lastName} 
                    onChange ={e => setLastName(e.target.value)} 
                    required
                />
                <input 
                    type="text" 
                    placeholder="Username" 
                    value={userName} 
                    onChange ={e => setUserName(e.target.value)} 
                    required
                />
                <input 
                    type="text" 
                    placeholder="Create a Password" 
                    value={password1} 
                    onChange ={e => setPassword1(e.target.value)} 
                    required
                />
                <input 
                    type="text" 
                    placeholder="Confirm Password" 
                    value={password2} 
                    onChange ={e => setPassword2(e.target.value)} 
                    required
                />
                {areFieldsComplete? 
                    <button type="submit" id="enabledBtn">Register</button> 
                    : 
                    <button id="disabledBtn" disabled>Register</button>
                }
                <Link to="/">Sign in</Link>
                {isRegistered?
                    <div id="successRegDiv">Registration successful. Redirecting to login page</div>
                    :
                    <div id="failedRegDiv">{message}</div>
                }
            </form>
        </div>
    )
}