import '../App.css';
import {Link, useNavigate} from 'react-router-dom';

import { useState, useEffect, useContext } from 'react';
import { UserContext } from '../UserContext';

export default function Login () {

    const [userName, setUserName] = useState('');
    const [password, setPassword] = useState('');
    const [isLoginDisabled, setIsLoginDisabled] = useState(true);
    const [isLoginSuccessful, setIsLoginSuccessful] = useState(false);
    const [message, setMessage] = useState('');
    const {setUser} = useContext(UserContext)

    const navigate = useNavigate();

    const loginUser = async (e) => {
        
        e.preventDefault();

        await fetch(`${process.env.REACT_APP_baseURL}/users/login`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                userName: userName,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === "User doesn't exist" || data === "Username or Password Incorrect"){
                
                setIsLoginSuccessful(false);
                setMessage("Username or Password Incorrect");
                
            }
            else{
                setIsLoginSuccessful(true);
                setMessage('Login successful')
                sessionStorage.setItem('token', data)
                getUserDetails(data)
                
                navigate("/tasks")
            }
        })
        .catch(error => console.error ('Error Login: ', error))

    }

    const getUserDetails = async (token) => {

        await fetch(`${process.env.REACT_APP_baseURL}/users/profile`, {
            headers: {'Authorization': `Bearer ${token}`}
        })
        .then(res => res.json())
        .then(data => {
            console.log('Login: ',data)
            setUser({
                userId: data._id,
                firstName: data.firstName,
                lastName: data.lastName,
                userName: data.userName
            })
        })
        .catch(err => console.error ('Error Fetching User Data: ', err))
    }

    useEffect(() => {
        if(userName === "" || password === ""){
            setIsLoginDisabled(true);
        }
        else{
            setIsLoginDisabled(false);
        }
    }, [userName, password])

    return(
        <div id="form-div">
            <form id="login-form" onSubmit={e => loginUser(e)}>
                <input 
                    type="text" 
                    placeholder="Username" 
                    value={userName} 
                    onChange={e => setUserName(e.target.value)} 
                    required
                />
                <input 
                    type="password" 
                    placeholder="Password" 
                    value={password} 
                    onChange={e => setPassword(e.target.value)} 
                    required
                />
                {isLoginDisabled? 
                    <><button disabled id="disabledLoginBtn">Login</button><br/> </>
                    :
                    <><button type="submit" id="enableLoginBtn">Login</button><br/></>
                }
                <Link to="/register">Click here to Sign up</Link>
                {isLoginSuccessful? 
                    <div id="successLoginDiv">{message}</div> 
                    : 
                    <div id="errorLoginDiv">{message}</div>
                }
            </form>
        </div>
    )
}