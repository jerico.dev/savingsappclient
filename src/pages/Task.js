import TaskList from "../components/TaskList";
import TaskDetails from "../components/TaskDetails";
import { useState } from "react";
import TaskTable from "../components/TaskTable";
import { Container, Row, Col } from "react-bootstrap";


export default function Task () {

    const [taskDetails, setTaskDetails] = useState(null);
    const [taskTable, setTaskTable] = useState(null)

    const handleTaskData = (taskData) => {
        setTaskDetails(taskData)
    }

    return(
        <Container>
            <Row>
                <Col>
                    <TaskList selectedTaskProp={handleTaskData}/>
                </Col>
                <Col>
                    {taskDetails && <TaskTable taskDetailsProp={taskDetails} taskTableProps={handleTaskData}/>}
                </Col>
                <Col>
                    {taskDetails && <TaskDetails taskDetailsProp={taskDetails} taskTableProps={taskDetails}/>}
                </Col>
            </Row>
        </Container>
    )
}